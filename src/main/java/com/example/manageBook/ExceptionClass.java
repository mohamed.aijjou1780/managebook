package com.example.manageBook;

public class ExceptionClass extends Exception{

    public ExceptionClass() {
        super();
    }

    public ExceptionClass(String s) {
        super(s);
    }

}

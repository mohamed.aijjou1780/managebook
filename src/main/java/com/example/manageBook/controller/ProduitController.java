package com.example.manageBook.controller;


import com.example.manageBook.model.Commente;
import com.example.manageBook.model.Produit;
import com.example.manageBook.model.Proprietaire;
import com.example.manageBook.repository.CommenteRepository;
import com.example.manageBook.services.ProduitService;
import com.example.manageBook.services.ProprietaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@CrossOrigin(origins = {"http://localhost:8100", "http://localhost:8080"})
public class ProduitController {

    @Autowired
    ProduitService produitService;

    @Autowired
    ProprietaireService proprietaireService;

    @Autowired
    CommenteRepository commenteRepository;


    @RequestMapping(value = "/produits", method = RequestMethod.GET)
    public List<Produit> getProduits() {
        List<Produit>produitList = new ArrayList<>();
        produitList = produitService.listDesProduits();
        return produitList;
    }

    @RequestMapping(value = "/produits/{idProduit}", method = RequestMethod.GET)
    public Produit chercherUnProduitParId(@PathVariable Integer idProduit) {

        Produit produit;
        if(idProduit != null){
            produit = produitService.trouverUnProduitParId(idProduit);
        }else{
            produit = null;
        }
        return produit;
    }

    @RequestMapping(value = "/proprietaire/{idProprietaire}/produit",  method = RequestMethod.POST,  produces = { "application/json" }, consumes = { "application/json" })
    public Produit creerUnProduit(@PathVariable Integer idProprietaire , @RequestBody Produit produit) {

        Produit produit1 = new Produit();
        Proprietaire proprietaire = proprietaireService.trouveUnProprietaireParId(idProprietaire);
        produit.setProprietaire(proprietaire);
        produit.setPresent("true");
        produit1 = produitService.creeUnProduit(produit);
        return produit1;
    }

    @RequestMapping(value = "/proprietaire/{idProprietaire}/search/barcode/{ean}", method = RequestMethod.GET)
    public Produit chercherUnProduitParEan(@PathVariable Integer idProprietaire, @PathVariable String ean) {

        Produit produit;
        if(ean != null){
            produit = produitService.trouverUnProduitParEan(ean,idProprietaire);
        }else{
            produit = null;
        }
        return produit;
    }

    @RequestMapping(value = "/proprietaire/{idProprietaire}/search/{idProduit}/delete", method = RequestMethod.DELETE)
    public ResponseEntity supprimeUnProduitParId(@PathVariable Integer idProprietaire, @PathVariable Integer idProduit) {

       List<Commente> listcommenteasupprime = commenteRepository.commenteParIdProduitEtProprietaire(idProprietaire, idProduit);
        Produit produit = produitService.trouverUnProduitParId(idProduit);
        if(idProduit != null && idProprietaire != null){
            if(listcommenteasupprime !=null) {
                for (Commente commente : listcommenteasupprime) {
                    commente.setProduit(null);
                    commente.setCommentaire(null);
                    commente.setNote(null);
                    commente.setProprietaire(null);
                }
            }
            produitService.deleteUnProduit(produit);

            Map<Object, Object> model = new HashMap<>();
            model.put("message", "Objet supprimé");
            return ok(model);
        }else{
            Map<Object, Object> model = new HashMap<>();
            model.put("message", "pas possible");
            return ok(model);

        }
    }
}

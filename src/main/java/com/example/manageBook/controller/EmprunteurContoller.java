package com.example.manageBook.controller;

import com.example.manageBook.ExceptionClass;
import com.example.manageBook.model.Emprunteur;
import com.example.manageBook.model.Produit;
import com.example.manageBook.services.EmprunteurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@CrossOrigin(origins = {"http://localhost:8100", "http://localhost:8080"})
public class EmprunteurContoller {


    @Autowired
    EmprunteurService emprunteurService;

    @RequestMapping(value = "/proprietaire/{id}/emprunteur", method = RequestMethod.GET)
    public List<Emprunteur> listDesEmprunteurs(@PathVariable Integer id) {

        List<Emprunteur> listEmprunteur = new ArrayList<>();

        if (id != 0) {

            listEmprunteur = emprunteurService.listEmprunteur();

            return listEmprunteur;

        } else {

            return null;
        }
    }

    @RequestMapping(value = "/proprietaire/{id}/emprunteuractif/{id2}", method = RequestMethod.GET)
    public Emprunteur chercheEmprunteurActif(@PathVariable Integer id, @PathVariable Integer id2) {

        Emprunteur emprunteur = emprunteurService.findEmprunteurByEmprunteAndProduitById(id2);

        return emprunteur;

    }

    @RequestMapping(value = "/proprietaire/{idProprietaire}/creationemprunteur", method = RequestMethod.POST, produces = {"application/json"}, consumes = {"application/json"})
    public ResponseEntity creeUnEmprunteur(@PathVariable Integer idProprietaire, @RequestBody Emprunteur emprunteur) {

        Emprunteur emprunteurACree = new Emprunteur();
        String nom = emprunteur.getNom();
        String prenom = emprunteur.getPrenom();

        Emprunteur emprunteurverif = emprunteurService.findEmprunteurByNomAndByPrenom(nom, prenom);

        if (emprunteurverif == null && emprunteur != null) {

            emprunteurACree = emprunteurService.creationEmprunteur(emprunteur);
            Map<Object, Object> model = new HashMap<>();
            model.put("message", "L Emprunteur a été crée");
            return ok(model);

        } else {

            Map<Object, Object> model = new HashMap<>();
            model.put("message", "L emprunteur existe déjà");
            return ok(model);

        }

    }

}
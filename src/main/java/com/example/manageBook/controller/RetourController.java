package com.example.manageBook.controller;

import com.example.manageBook.model.Emprunte;
import com.example.manageBook.model.Emprunteur;
import com.example.manageBook.model.Produit;
import com.example.manageBook.model.Retour;
import com.example.manageBook.services.EmprunteService;
import com.example.manageBook.services.EmprunteurService;
import com.example.manageBook.services.ProduitService;
import com.example.manageBook.services.RetourService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = {"http://localhost:8100", "http://localhost:8080"})
public class RetourController {


    @Autowired
    RetourService retourService;

    @Autowired
    EmprunteService emprunteService;


    @Autowired
    EmprunteurService emprunteurService;

    @Autowired
    ProduitService produitService;


    @RequestMapping(value = "/retour/{id2}/{id3}", method = RequestMethod.GET)
    public Retour creeUnRetour(@PathVariable Integer id2, @PathVariable Integer id3){

        String dispo = "true";

        Date date = new Date();

        Retour retour = new Retour();
        Retour retourACreer = new Retour();

    Emprunteur emprunteur = emprunteurService.findEmprunteurById(id2);
    Produit produit = produitService.trouverUnProduitParId(id3);

    produit.setPresent(dispo);

    Produit produit1 = produitService.creeUnProduit(produit);

    retourACreer.setEmprunteur(emprunteur);
    retourACreer.setProduit(produit1);
    retourACreer.setDate(date);

    retour = retourService.creeUnRetour(retourACreer);

    Emprunte emprunteASupprimer = emprunteService.getEmpruntByIdEmprunteurByIdProduit(id2, id3);

    Integer idEmprunteASupprimer  = emprunteASupprimer.getId();

    emprunteASupprimer.setEmprunteur(null);
    emprunteASupprimer.setProduit(null);
    emprunteASupprimer.setDate(null);

    this.emprunteService.supprimeUnEmpruntById(idEmprunteASupprimer);
    return retour;
    }
}

package com.example.manageBook.controller;

import com.example.manageBook.model.Produit;
import com.example.manageBook.model.Proprietaire;
import com.example.manageBook.services.ProduitService;
import com.example.manageBook.services.ProprietaireService;
import com.example.manageBook.services.impl.ProprietaireServiceDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = {"http://localhost:8100", "http://localhost:8080"})
public class ProprietaireController {


    @Autowired
    ProprietaireService proprietaireService;

    @Autowired
    ProduitService produitService;


    @Autowired
    ProprietaireServiceDetails proprietaireServiceDetails;

    @RequestMapping(value = "/proprietaire", method = RequestMethod.POST, consumes =  MediaType.APPLICATION_JSON_VALUE)
    public Proprietaire creationProprietaire(@RequestBody Proprietaire proprietaire) {

        Proprietaire proprietaire1 = new Proprietaire();

        proprietaire1 = proprietaireService.creeUnProprietaire(proprietaire);

        return proprietaire1;

    }

    @RequestMapping(value = "/proprietaire/{id}", method = RequestMethod.GET)
    public Proprietaire chercherUnProprietaireParId(@PathVariable Integer id) {

        Proprietaire proprietaire;

        if (id != null) {

            proprietaire = proprietaireService.trouveUnProprietaireParId(id);

        } else {

            proprietaire = null;
        }

        return proprietaire;

    }

    @RequestMapping(value = "/proprietaires/{identifiant}", method = RequestMethod.GET)
    public Proprietaire chercherUnProprietaireParIdentiant(@PathVariable String identifiant) {

        Proprietaire proprietaire;

        if (identifiant != null) {

            proprietaire = proprietaireServiceDetails.findProprietaireByIdentifiant(identifiant);

        } else {

            proprietaire = null;
        }

        return proprietaire;

    }

    @RequestMapping(value = "/proprietaireid/{identifiant}", method = RequestMethod.GET)
    public Integer chercherIdProprietaireParIdentiant(@PathVariable String identifiant) {

        Integer IdProprietaire;

        if (identifiant != null) {

            IdProprietaire = proprietaireServiceDetails.findIdProprietaireByIdentifiant(identifiant);

        } else {

            IdProprietaire = null;
        }

        return IdProprietaire;

    }

    @RequestMapping(value = "/proprietaire/{id}/produits", method = RequestMethod.GET)
    public List<Produit> rechercheProduitsParidProprietaire(@PathVariable Integer id) {

        List<Produit> listproduit = new ArrayList<>();

        if (id != 0 && id != null) {

            listproduit = produitService.donneLaListeDesProduitsParProprietaire(id);

            return listproduit;
        }

        if (id == null) {


            return null;

        }
        return listproduit;

    }

    @RequestMapping(value = "/proprietaire/{id}/produitsdispo", method = RequestMethod.GET)
    public List<Produit> rechercheProduitsdispo(@PathVariable Integer id) {

        List<Produit> listproduit = new ArrayList<>();

        if (id != 0 && id != null) {

            listproduit = produitService.donneLaListeDesProduitsDispo(id);

            return listproduit;
        }

        if (id == null) {


            return null;

        }
        return listproduit;

    }

    @RequestMapping(value = "/proprietaire/{id}/produitsnondispo", method = RequestMethod.GET)
    public List<Produit> rechercheProduitsNonDispo(@PathVariable Integer id) {

        List<Produit> listproduitnondispo = new ArrayList<>();

        if (id != 0 && id != null) {

            listproduitnondispo = produitService.donneLaListeDesProduitsNonDispo(id);

            return listproduitnondispo;
        }

        if (id == null) {


            return null;

        }
        return listproduitnondispo;

    }

    @RequestMapping(value = "/proprietaire/{id}/produits/{id2}", method = RequestMethod.GET)
    public Produit rechercheUnProduitEtParIdParidProprietaire(@PathVariable Integer id, @PathVariable Integer id2) {

        Produit produit = new Produit();

        if (id != 0 && id != null && id2 !=null && id2 !=0) {

            produit = produitService.donneUnProduitParIdEtParProprietaire(id, id2);

            return produit;
        }

        if (id == null && id2 == null) {


            return null;

        }
        return produit;

    }

}









package com.example.manageBook.controller;

import com.example.manageBook.model.*;
import com.example.manageBook.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@CrossOrigin(origins = {"http://localhost:8100", "http://localhost:8080"})
public class CommenteController {


    @Autowired
    EmprunteService emprunteService;

    @Autowired
    EmprunteurService emprunteurService;

    @Autowired
    ProduitService produitService;

    @Autowired
    CommenteService commenteService;

    @Autowired
    ProprietaireService proprietaireService;


    @RequestMapping(value = "/proprietaire/{id}/produits/{id2}/commente", method = RequestMethod.GET)
    public Commente creeUnCommente(@PathVariable Integer id, @PathVariable Integer id2, @RequestParam Integer note, @RequestParam String commentaire){


        Commente commente = new Commente();
        Commente commente1 = new Commente();

        Date date = new Date();
        Proprietaire proprietaire = proprietaireService.trouveUnProprietaireParId(id);
        Produit  produit = produitService.trouverUnProduitParId(id2);

        commente.setDate(date);
        commente.setNote(note);
        commente.setCommentaire(commentaire);
        commente.setEmprunteur(null);
        commente.setProprietaire(proprietaire);
        commente.setProduit(produit);

        commente1 = commenteService.creeUnCommente(commente);


        return commente1;
    }

    @RequestMapping(value="/proprietaire/{id}/produits/{id2}/listecommentaire", method = RequestMethod.GET)
    public List<Commente> retourneListCommenteParIdProduit(@PathVariable Integer id, @PathVariable Integer id2){


        List<Commente> commenteList = new ArrayList<Commente>();

            commenteList = commenteService.getCommenteByIdProduitAndByProprietaire(id,id2);

            return commenteList;
    }


}

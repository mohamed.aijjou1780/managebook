package com.example.manageBook.controller;

import com.example.manageBook.model.Emprunte;
import com.example.manageBook.model.Emprunteur;
import com.example.manageBook.model.KeyEmprunte;
import com.example.manageBook.model.Produit;
import com.example.manageBook.services.EmprunteService;
import com.example.manageBook.services.EmprunteurService;
import com.example.manageBook.services.ProduitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.metamodel.Metamodel;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@CrossOrigin(origins = {"http://localhost:8100", "http://localhost:8080"})
public class EmprunteController {


    @Autowired
    EmprunteService emprunteService;

    @Autowired
    EmprunteurService emprunteurService;

    @Autowired
    ProduitService produitService;


    @RequestMapping(value = "/emprunte/{id2}/{id3}", method = RequestMethod.GET)
    public Emprunte creeUnEmprunt(@PathVariable Integer id2, @PathVariable Integer id3){

    Emprunte emprunte = new Emprunte();

    String dispo = "false";

    Date date = new Date();

    Emprunteur emprunteur = emprunteurService.findEmprunteurById(id2);
    Produit produit = produitService.trouverUnProduitParId(id3);

    produit.setPresent("false");

    Produit produit1 = produitService.creeUnProduit(produit);

    emprunte.setEmprunteur(emprunteur);
    emprunte.setProduit(produit1);
    emprunte.setDate(date);

    Emprunte emprunte1 = emprunteService.creeUnEmprunt(emprunte);

    return emprunte1;

    }

    @RequestMapping(value = "/empruntes", method = RequestMethod.GET)
    public List<Emprunte> getListemprunt() {

        List<Emprunte> emprunteList = emprunteService.getListEmprunt();

        return emprunteList;

    }


    @RequestMapping(value = "/emprunte/{idEmprunte}", method = RequestMethod.GET)
    public Emprunte getListempruntById(@PathVariable Integer idEmprunte) {

        Emprunte emprunte;

        emprunte = emprunteService.getListEmpruntById(idEmprunte);

        return emprunte;

    }


    @RequestMapping(value = "/empruntedate/{id1}", method = RequestMethod.GET)
    public Date recupereUneDateEmpruntIdProduit( @PathVariable Integer id1) {

        Date dateEmprunt;

        dateEmprunt = emprunteService.getDateEmpruntByIdProduit(id1);
        if (dateEmprunt != null) {

            return dateEmprunt;
        } else {

            return null;
        }


    }


}

package com.example.manageBook.controller;


import com.example.manageBook.JwtTokenProvider;
import com.example.manageBook.model.Proprietaire;
import com.example.manageBook.services.ProprietaireService;
import com.example.manageBook.services.impl.ProprietaireServiceDetails;
import com.example.manageBook.services.impl.ProprietaireServiceLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.ok;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(origins = {"http://localhost:8100", "http://localhost:8080"})
public class AuthController {


    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider jwtTokenProvider;



    @Autowired
    ProprietaireServiceDetails proprietaireServiceDetails;

    @Autowired
    ProprietaireServiceLog proprietaireServiceLog;

    @Autowired
    ProprietaireService proprietaireService;


    private final PasswordEncoder passwordEncoder;

    private final String userNotFoundEncodedPassword;

    public AuthController(PasswordEncoder passwordEncoder, ProprietaireServiceLog proprietaireServiceLog ,
                          JwtTokenProvider jwtTokenProvider) {
        this.proprietaireServiceLog = proprietaireServiceLog;
        this.jwtTokenProvider = jwtTokenProvider;
        this.passwordEncoder = passwordEncoder;

        this.userNotFoundEncodedPassword = this.passwordEncoder
                .encode("userNotFoundPassword");
    }

    @GetMapping("/authenticate")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void authenticate() {

    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity authorize(@Valid @RequestBody Proprietaire loginUser) {

        Proprietaire proprietaire = this.proprietaireServiceLog.lookup(loginUser.getIdentifiant());
        if (proprietaire != null) {
            boolean pwMatches = this.passwordEncoder.matches(loginUser.getMdp(),
                    proprietaire.getMdp());
            if (pwMatches) {
                Map<Object, Object> model = new HashMap<>();
                model.put("message", "login successfully");
                return ok(model);
            }
        }
        else {
            this.passwordEncoder.matches(loginUser.getMdp(),
                    this.userNotFoundEncodedPassword);
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity signup(@RequestBody Proprietaire signupUser) {

        if (this.proprietaireServiceLog.usernameExists(signupUser.getIdentifiant()) == true) {
            throw new BadCredentialsException("User with username: " + signupUser.getIdentifiant() + " already");

        }else {

            signupUser.encodePassword(this.passwordEncoder);
            this.proprietaireServiceLog.creeUnProprietaire(signupUser);
            this.jwtTokenProvider.createToken(signupUser.getIdentifiant());

            Map<Object, Object> model = new HashMap<>();
            model.put("message", "User registered successfully");
            return ok(model);
        }
    }



}
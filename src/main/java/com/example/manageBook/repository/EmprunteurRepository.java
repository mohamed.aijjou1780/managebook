package com.example.manageBook.repository;

import com.example.manageBook.model.Emprunte;
import com.example.manageBook.model.Emprunteur;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface EmprunteurRepository extends CrudRepository<Emprunteur, Integer> {


    @Query("select e FROM Emprunteur e WHERE e.id=:id")
    public Emprunteur findEmprunteurParId(@Param("id") Integer id);

    @Query("select e FROM Emprunteur e JOIN Emprunte em ON em.emprunteur.id = e.id JOIN Produit p ON p.id = em.produit.id WHERE p.id=:id")
    public Emprunteur findEmprunteurByEmprunteAndProduitById(@Param("id") Integer id);

    @Query("select e FROM Emprunteur e WHERE e.nom = :nom and e.prenom = :prenom")
    public Emprunteur findEmprunteurByNomAndPrenom(@Param("nom") String nom, @Param("prenom") String prenom);

}

package com.example.manageBook.repository;

import com.example.manageBook.model.Emprunteur;
import com.example.manageBook.model.Produit;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Transactional
public interface ProduitRepository extends CrudRepository<Produit, Integer> {


    @Query("select e FROM Produit e WHERE e.proprietaire.id=:id")
    public List<Produit>listproduitParProprietaire(@Param("id") Integer id);

    @Query("select e FROM Produit e WHERE e.proprietaire.id=:id and e.id=:id2")
    public Produit trouveUnProduitParIdEtParProprietaire(@Param("id") Integer id, @Param("id2") Integer id2);

    @Query("select e FROM Produit e WHERE e.present = 'true' and e.proprietaire.id=:id")
    public List<Produit> donneLalisteDesProduitsDispo(@Param("id") Integer id);

    @Query("select e FROM Produit e WHERE e.present = 'false' and e.proprietaire.id=:id")
    public List<Produit> donneLalisteDesProduitsNonDispo(@Param("id") Integer id);

    @Query("select e FROM Produit e WHERE e.id=:id")
    public Produit findProduitParId(@Param("id") Integer id);

    @Query("select p FROM Produit p WHERE p.ean=:ean and p.proprietaire.id=:id")
    public Produit findProduitParEan(@Param("ean") String ean, @Param("id") Integer id);

//    @Query("delete from Produit p where p.id:id")
//    public void deleteProduitById(@Param("id") Integer id);



    /*
    @Transactional
    @Modifying
    @Query("select e from "
    */


}

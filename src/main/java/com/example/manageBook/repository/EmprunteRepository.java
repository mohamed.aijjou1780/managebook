package com.example.manageBook.repository;

import com.example.manageBook.model.Emprunte;
import com.example.manageBook.model.KeyEmprunte;
import com.example.manageBook.model.Produit;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;


@Transactional
public interface EmprunteRepository extends CrudRepository<Emprunte, Integer> {


    @Query("select e FROM Emprunte e WHERE e.id=:id")
    public Emprunte listemprunteParId(@Param("id") Integer id);

    @Query("select DISTINCT e FROM Emprunte e WHERE e.produit.id=:id1 and e.emprunteur.id=:id2")
    public Emprunte unEmpruntParIdEmprunteurEtIdProduit(@Param("id2") Integer id2, @Param("id1") Integer id1);

    @Query("select e.date FROM Emprunte e WHERE e.produit.id=:id1")
    public Date uneDateEmpruntParIdProduit(@Param("id1") Integer id1);

}

package com.example.manageBook.repository;

import com.example.manageBook.model.Emprunte;
import com.example.manageBook.model.KeyEmprunte;
import com.example.manageBook.model.Retour;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


@Transactional
public interface RetourRepository extends CrudRepository<Retour, Integer> {


    @Query("select r FROM Retour r WHERE r.id=:id")
    public Retour listeRetourParId(@Param("id") Integer id);






}

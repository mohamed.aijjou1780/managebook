package com.example.manageBook.repository;

import com.example.manageBook.model.Commente;
import com.example.manageBook.model.Retour;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Transactional
public interface CommenteRepository extends CrudRepository<Commente, Integer> {


    @Query("select c FROM Commente c WHERE c.id=:id")
    public Commente commenteParId(@Param("id") Integer id);


    @Query("select c FROM Commente c WHERE  c.proprietaire.id=:id and c.produit.id=:id2")
    public List<Commente> commenteParIdProduitEtProprietaire(@Param("id") Integer id, @Param("id2") Integer id2);




}

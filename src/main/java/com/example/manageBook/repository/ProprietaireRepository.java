package com.example.manageBook.repository;

import com.example.manageBook.model.Produit;
import com.example.manageBook.model.Proprietaire;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


@Transactional
public interface ProprietaireRepository extends CrudRepository<Proprietaire, Integer> {


    @Query("select e FROM Proprietaire e WHERE e.id=:id")
    public Proprietaire findProprietaireParId(@Param("id") Integer id);

    @Query("select e FROM Proprietaire e WHERE e.identifiant=:identifiant")
    public Proprietaire findProprietaireByIdentiant(@Param("identifiant") String Identifiant);

    @Query("select e.id FROM Proprietaire e WHERE e.identifiant=:identifiant")
    public Integer findIdProprietaireByIdentiant(@Param("identifiant") String Identifiant);

    /*
    @Transactional
    @Modifying
    @Query("select e from "
    */


}

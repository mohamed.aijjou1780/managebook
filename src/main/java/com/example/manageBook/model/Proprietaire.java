package com.example.manageBook.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class Proprietaire {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "description")
    private String description;

    @Column(name = "idenfiant")
    private String identifiant;

    @Column(name = "mdp")
    private String mdp;





    @JsonBackReference("commente")
    @OneToMany(mappedBy = "proprietaire", fetch = FetchType.EAGER)
    private List<Commente> listCommentaire;

    public Proprietaire (){}

    public Proprietaire(Integer id, String nom, String prenom, String avatar, String description, String identifiant, String mdp, List<Commente> listCommentaire) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.avatar = avatar;
        this.description = description;
        this.identifiant = identifiant;
        this.mdp = mdp;

        this.listCommentaire = listCommentaire;
    }

    @Override
    public String toString() {
        return "Proprietaire{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", avatar='" + avatar + '\'' +
                ", description='" + description + '\'' +
                ", identifiant='" + identifiant + '\'' +
                ", mdp='" + mdp + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }



    public List<Commente> getListCommentaire() {
        return listCommentaire;
    }

    public void setListCommentaire(List<Commente> listCommentaire) {
        this.listCommentaire = listCommentaire;
    }

    public void encodePassword(PasswordEncoder passwordEncoder) {
        this.mdp = passwordEncoder.encode(this.mdp);
    }
}

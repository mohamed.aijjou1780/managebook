package com.example.manageBook.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class KeyCommente implements Serializable {


    @Column(name = "idproprietaire")
    Integer idproprietaire;

    @Column(name = "idproduit")
    Integer idproduit;

    @Column(name = "idemprunteur")
    Integer idemprunteur;

    public KeyCommente(){}

    public KeyCommente(Integer idproprietaire, Integer idproduit, Integer idemprunteur) {
        this.idproprietaire = idproprietaire;
        this.idproduit = idproduit;
        this.idemprunteur = idemprunteur;
    }

    public Integer getIdproprietaire() {
        return idproprietaire;
    }

    public void setIdproprietaire(Integer idproprietaire) {
        this.idproprietaire = idproprietaire;
    }

    public Integer getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(Integer idproduit) {
        this.idproduit = idproduit;
    }

    public Integer getIdemprunteur() {
        return idemprunteur;
    }

    public void setIdemprunteur(Integer idemprunteur) {
        this.idemprunteur = idemprunteur;
    }
}

package com.example.manageBook.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Retour {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "date")
    private Date date;



    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.PERSIST)
    @PrimaryKeyJoinColumn(name = "produit_id", referencedColumnName = "id")
    Produit produit;


    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.PERSIST)
    @PrimaryKeyJoinColumn(name = "emprunteur_id", referencedColumnName = "id")
    Emprunteur emprunteur;


    public Retour(){}

    public Retour(Integer id, Date date, Produit produit, Emprunteur emprunteur) {
        this.id = id;
        this.date = date;
        this.produit = produit;
        this.emprunteur = emprunteur;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Emprunteur getEmprunteur() {
        return emprunteur;
    }

    public void setEmprunteur(Emprunteur emprunteur) {
        this.emprunteur = emprunteur;
    }
}

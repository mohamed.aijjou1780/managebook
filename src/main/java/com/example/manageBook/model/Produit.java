package com.example.manageBook.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Produit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="titre")
    private String titre;

    @Column(name="auteur")
    private String auteur;

    @Column(name="type")
    private String type;

    @Column(name="image")
    private String image;

    @Column(name="image2")
    private String image2;

    @Column(name="ean")
    private String ean;

    @Column(name="edition")
    private String edition;

    @Column(name="description")
    private String description;

    @Column(name="present")
    private String present;

    @Column(name="date")
    private Date date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "idproprietaire", referencedColumnName = "id")
    private Proprietaire proprietaire;

    public Produit(Integer id, String titre, String auteur, String type, String image, String image2, String ean, String edition, String description, String present, Date date, Proprietaire proprietaire, List<Commente> listCommentaire1, List<Emprunte> listEmprunt1, List<Retour> listRetour) {
        this.id = id;
        this.titre = titre;
        this.auteur = auteur;
        this.type = type;
        this.image = image;
        this.image2 = image2;
        this.ean = ean;
        this.edition = edition;
        this.description = description;
        this.present = present;
        this.date = date;
        this.proprietaire = proprietaire;
        this.listCommentaire1 = listCommentaire1;
        this.listEmprunt1 = listEmprunt1;
        this.listRetour = listRetour;
    }

    @JsonBackReference("commente")
    @OneToMany(mappedBy = "produit", fetch = FetchType.EAGER)
    private List<Commente> listCommentaire1;

    @JsonBackReference("emprunte")
    @OneToMany(mappedBy = "produit", fetch = FetchType.EAGER)
    private List<Emprunte> listEmprunt1;

    @JsonBackReference("retour")
    @OneToMany(mappedBy = "produit", fetch = FetchType.EAGER)
    private List<Retour> listRetour;



    public Produit(){

    }

    @Override
    public String toString() {
        return "Produit{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", auteur='" + auteur + '\'' +
                ", type='" + type + '\'' +
                ", image='" + image + '\'' +
                ", image2='" + image2 + '\'' +
                ", ean='" + ean + '\'' +
                ", edition='" + edition + '\'' +
                ", description='" + description + '\'' +
                ", present='" + present + '\'' +
                ", date=" + date +
                ", proprietaire=" + proprietaire +
                ", listEmprunt1=" + listEmprunt1 +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPresent() {
        return present;
    }

    public void setPresent(String present) {
        this.present = present;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Proprietaire getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Proprietaire proprietaire) {
        this.proprietaire = proprietaire;
    }

    public List<Commente> getListCommentaire1() {
        return listCommentaire1;
    }

    public void setListCommentaire1(List<Commente> listCommentaire1) {
        this.listCommentaire1 = listCommentaire1;
    }

    public List<Emprunte> getListEmprunt1() {
        return listEmprunt1;
    }

    public void setListEmprunt1(List<Emprunte> listEmprunt1) {
        this.listEmprunt1 = listEmprunt1;
    }

    public List<Retour> getListRetour() {
        return listRetour;
    }

    public void setListRetour(List<Retour> listRetour) {
        this.listRetour = listRetour;
    }
}

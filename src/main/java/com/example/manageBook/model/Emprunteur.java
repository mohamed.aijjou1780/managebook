package com.example.manageBook.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;
@Entity
public class Emprunteur {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="nom")
    private String nom;

    @Column(name="prenom")
    private String prenom;

    @Column(name="lien")
    private String lien;

    @JsonBackReference("commente")
    @OneToMany(mappedBy = "emprunteur", fetch = FetchType.EAGER)
    private List<Commente> listCommentaire2;

    @JsonBackReference("emprunte")
    @OneToMany(mappedBy = "emprunteur", fetch = FetchType.EAGER)
    private List<Emprunte> listEmprunt;

    @JsonBackReference("retour")
    @OneToMany(mappedBy = "emprunteur", fetch = FetchType.EAGER)
    private List<Retour> listRetour;

    public Emprunteur (){}

    public Emprunteur(Integer id, String nom, String prenom, String lien, List<Commente> listCommentaire2, List<Emprunte> listEmprunt, List<Retour> listRetour) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.lien = lien;
        this.listCommentaire2 = listCommentaire2;
        this.listEmprunt = listEmprunt;
        this.listRetour = listRetour;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getLien() {
        return lien;
    }

    public void setLien(String lien) {
        this.lien = lien;
    }

    public List<Commente> getListCommentaire2() {
        return listCommentaire2;
    }

    public void setListCommentaire2(List<Commente> listCommentaire2) {
        this.listCommentaire2 = listCommentaire2;
    }

    public List<Emprunte> getListEmprunt() {
        return listEmprunt;
    }

    public void setListEmprunt(List<Emprunte> listEmprunt) {
        this.listEmprunt = listEmprunt;
    }

    public List<Retour> getListRetour() {
        return listRetour;
    }

    public void setListRetour(List<Retour> listRetour) {
        this.listRetour = listRetour;
    }

    @Override
    public String toString() {
        return "Emprunteur{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", lien='" + lien + '\'' +
                '}';
    }
}

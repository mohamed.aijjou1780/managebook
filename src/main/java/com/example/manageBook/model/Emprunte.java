package com.example.manageBook.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Date;
import java.util.Optional;

@Entity
public class Emprunte {


//    @EmbeddedId
//    private KeyEmprunte id;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "date")
    private Date date;


    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.PERSIST)
    @PrimaryKeyJoinColumn(name = "emprunteur_id", referencedColumnName = "id")
    Emprunteur emprunteur;


    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.PERSIST)
    @PrimaryKeyJoinColumn(name = "produit_id", referencedColumnName = "id")
    Produit produit;

    public Emprunte(){}

    public Emprunte(Integer id, Date date, Emprunteur emprunteur, Produit produit) {
        this.id = id;
        this.date = date;
        this.emprunteur = emprunteur;
        this.produit = produit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Emprunteur getEmprunteur() {
        return emprunteur;
    }

    public void setEmprunteur(Emprunteur emprunteur) {
        this.emprunteur = emprunteur;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }
}

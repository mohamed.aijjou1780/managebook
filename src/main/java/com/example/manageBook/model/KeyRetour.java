package com.example.manageBook.model;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class KeyRetour implements Serializable {

    @Column(name = "idproduit")
    Integer idproduit;

    @Column(name = "idemprunteur")
    Integer idemprunteur;

    public KeyRetour(Integer idproduit, Integer idemprunteur) {
        this.idproduit = idproduit;
        this.idemprunteur = idemprunteur;
    }

    public  KeyRetour(){}

    public Integer getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(Integer idproduit) {
        this.idproduit = idproduit;
    }

    public Integer getIdemprunteur() {
        return idemprunteur;
    }

    public void setIdemprunteur(Integer idemprunteur) {
        this.idemprunteur = idemprunteur;
    }


}

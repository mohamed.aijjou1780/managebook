package com.example.manageBook.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Commente {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "date")
    private Date date;

    @Column(name = "note")
    private Integer note;

    @Column(name = "commentaire")
    private String commentaire;


    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.PERSIST)
    @PrimaryKeyJoinColumn(name = "proprietaire_id", referencedColumnName = "id")
    Proprietaire proprietaire;


    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.PERSIST)
    @PrimaryKeyJoinColumn(name = "produit_id", referencedColumnName = "id")
    Produit produit;


    @ManyToOne(fetch = FetchType.EAGER, cascade=CascadeType.PERSIST)
    @PrimaryKeyJoinColumn(name = "emprunteur_id", referencedColumnName = "id")
    Emprunteur emprunteur;

    public Commente(Integer id, Date date, Integer note, String commentaire, Proprietaire proprietaire, Produit produit, Emprunteur emprunteur) {
        this.id = id;
        this.date = date;
        this.note = note;
        this.commentaire = commentaire;
        this.proprietaire = proprietaire;
        this.produit = produit;
        this.emprunteur = emprunteur;
    }

    public Commente (){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getNote() {
        return note;
    }

    public void setNote(Integer note) {
        this.note = note;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Proprietaire getProprietaire() {
        return proprietaire;
    }

    public void setProprietaire(Proprietaire proprietaire) {
        this.proprietaire = proprietaire;
    }

    public Produit getProduit() {
        return produit;
    }

    public void setProduit(Produit produit) {
        this.produit = produit;
    }

    public Emprunteur getEmprunteur() {
        return emprunteur;
    }

    public void setEmprunteur(Emprunteur emprunteur) {
        this.emprunteur = emprunteur;
    }

    @Override
    public String toString() {
        return "Commente{" +
                "id=" + id +
                ", date=" + date +
                ", note=" + note +
                ", commentaire='" + commentaire + '\'' +
                ", proprietaire=" + proprietaire +
                ", produit=" + produit +
                ", emprunteur=" + emprunteur +
                '}';
    }
}

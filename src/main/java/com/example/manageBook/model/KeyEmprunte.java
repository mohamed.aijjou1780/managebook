package com.example.manageBook.model;


import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class KeyEmprunte implements Serializable {


    @Column(name = "idemprunteur")
    Integer idemprunteur;

    @Column(name = "idproduit")
    Integer idproduit;


    public KeyEmprunte(Integer idproprietaire, Integer idproduit) {
        this.idemprunteur = idproprietaire;
        this.idproduit = idproduit;
    }

    public KeyEmprunte(){

    }

    public Integer getIdemprunteur() {
        return idemprunteur;
    }

    public void setIdemprunteur(Integer idemprunteur) {
        this.idemprunteur = idemprunteur;
    }

    public Integer getIdproduit() {
        return idproduit;
    }

    public void setIdproduit(Integer idproduit) {
        this.idproduit = idproduit;
    }

    @Override
    public String toString() {
        return "KeyEmprunte{" +
                "idemprunteur=" + idemprunteur +
                ", idproduit=" + idproduit +
                '}';
    }
}

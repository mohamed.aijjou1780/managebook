package com.example.manageBook.services;

import com.example.manageBook.model.Emprunteur;

import java.util.List;
import java.util.Optional;

public interface EmprunteurService {


    public List<Emprunteur> listEmprunteur();
    public Emprunteur findEmprunteurById(Integer idEmprunteur);
    public Emprunteur findEmprunteurByEmprunteAndProduitById(Integer idProduit);
    public Emprunteur findEmprunteurByNomAndByPrenom(String nom, String prenom);
    public Emprunteur creationEmprunteur(Emprunteur emprunteur);

}

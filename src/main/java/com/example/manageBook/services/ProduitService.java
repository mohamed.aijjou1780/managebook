package com.example.manageBook.services;

import com.example.manageBook.model.Produit;

import java.util.List;
import java.util.Optional;

public interface ProduitService {

    List<Produit> listDesProduits();
    public Produit trouverUnProduitParId(Integer idProduit);
    List<Produit> donneLaListeDesProduitsParProprietaire(Integer idProprietaire);
    Produit donneUnProduitParIdEtParProprietaire(Integer idProprietaire, Integer idProduit);
    List<Produit> donneLaListeDesProduitsDispo(Integer idProprietaire);
    public Produit creeUnProduit(Produit produit);
    public List<Produit> donneLaListeDesProduitsNonDispo(Integer idProprietaire);
    Produit trouverUnProduitParEan(String ean, Integer id);
    void deleteUnProduitById(Integer idProduit);
    public void deleteUnProduit(Produit produit);

}

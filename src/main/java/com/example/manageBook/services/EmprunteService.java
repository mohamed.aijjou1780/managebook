package com.example.manageBook.services;

import com.example.manageBook.model.Emprunte;
import com.example.manageBook.model.KeyEmprunte;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface EmprunteService {



    Emprunte creeUnEmprunt(Emprunte emprunte);
    List<Emprunte> getListEmprunt();
    Emprunte getListEmpruntById(Integer idEmprunte);
    Emprunte getEmpruntByIdEmprunteurByIdProduit(Integer idEmprunteur, Integer idProduit);
    void supprimeUnEmpruntById(Integer idEmprunte);
    Date getDateEmpruntByIdProduit(Integer idProduit);

}

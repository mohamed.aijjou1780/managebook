package com.example.manageBook.services.impl;

import com.example.manageBook.model.Proprietaire;
import com.example.manageBook.repository.ProprietaireRepository;
import com.example.manageBook.services.ProprietaireService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Component
@Transactional
public class ProprietaireServiceImpl implements ProprietaireService {



    @Resource
    private ProprietaireRepository proprietaireRepository;





    @Override
    public Proprietaire creeUnProprietaire(Proprietaire proprietaire){

    Proprietaire proprietaire1 = proprietaireRepository.save(proprietaire);


        return proprietaire1;
    }

    @Override
    public Proprietaire trouveUnProprietaireParId(Integer idProprietaire){

        Proprietaire proprietaire1;

        if (idProprietaire != null && idProprietaire!= 0){

             proprietaire1 = proprietaireRepository.findProprietaireParId(idProprietaire);

            return proprietaire1;

        }else{

            return null;
        }

    }



}

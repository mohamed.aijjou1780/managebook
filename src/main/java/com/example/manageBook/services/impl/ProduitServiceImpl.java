package com.example.manageBook.services.impl;

import com.example.manageBook.model.Produit;
import com.example.manageBook.repository.ProduitRepository;
import com.example.manageBook.services.ProduitService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Component
@Transactional
public class ProduitServiceImpl implements ProduitService {



@Resource
    private ProduitRepository produitRepository;


@Override
    public List<Produit>listDesProduits(){

    Iterable<Produit>listProduit1 = produitRepository.findAll();
    List<Produit>listProduit2 = new ArrayList<Produit>();

    for(Produit produit : listProduit1){

        listProduit2.add(produit);
    }

    return listProduit2;
}

    @Override
    public Produit trouverUnProduitParId(Integer idProduit){

    Produit produit = new Produit();

        if(idProduit != null){
            produit = produitRepository.findProduitParId(idProduit);
        }else{

          produit = null;

        }

        return produit;
    }

    @Override
    public List<Produit> donneLaListeDesProduitsParProprietaire(Integer idProprietaire){

    List<Produit>listeProduit = produitRepository.listproduitParProprietaire(idProprietaire);

    return  listeProduit;

    }

    @Override
    public List<Produit> donneLaListeDesProduitsDispo(Integer idProprietaire){

        List<Produit>listeProduit = produitRepository.donneLalisteDesProduitsDispo(idProprietaire);

        return  listeProduit;

    }

    @Override
    public List<Produit> donneLaListeDesProduitsNonDispo(Integer idProprietaire){

        List<Produit>listeProduit = produitRepository.donneLalisteDesProduitsNonDispo(idProprietaire);

        return  listeProduit;

    }

    @Override
    public Produit donneUnProduitParIdEtParProprietaire(Integer idProprietaire, Integer idProduit){

        Produit produit = produitRepository.trouveUnProduitParIdEtParProprietaire(idProprietaire, idProduit);

        return  produit;

    }

    @Override
    public Produit creeUnProduit(Produit produit){

        Produit produit1 = produitRepository.save(produit);

        return  produit1;

    }

    @Override
    public Produit trouverUnProduitParEan(String ean, Integer id){

        Produit produit = new Produit();

        if(ean != null){
            produit = produitRepository.findProduitParEan(ean, id);
        }else{

            produit = null;

        }

        return produit;
    }


    @Override
    public void deleteUnProduitById(Integer idProduit){

//       this.produitRepository.deleteProduitById(idProduit);

        this.produitRepository.deleteById(idProduit);

    }

    @Override
    public void deleteUnProduit(Produit produit){

//       this.produitRepository.deleteProduitById(idProduit);

       this.produitRepository.delete(produit);

    }

}

package com.example.manageBook.services.impl;

import com.example.manageBook.model.Commente;
import com.example.manageBook.repository.CommenteRepository;
import com.example.manageBook.services.CommenteService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class CommenteServiceImpl implements CommenteService {


    @Resource
    CommenteRepository commenteRepository;


    @Override
    public Commente creeUnCommente(Commente commente){

        Commente commente1 = commenteRepository.save(commente);

                return commente1;
    }


    @Override
    public List<Commente> getCommenteByIdProduitAndByProprietaire( Integer idProprietaire, Integer idProduit){

        List<Commente> listecommente;

        listecommente = commenteRepository.commenteParIdProduitEtProprietaire(idProprietaire, idProduit);

        return listecommente;
    }

    @Override
    public void supprimeUnCommenteById(Integer idCommente){


    this.commenteRepository.deleteById(idCommente);


    }



}

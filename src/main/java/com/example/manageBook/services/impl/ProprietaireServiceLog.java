package com.example.manageBook.services.impl;

import com.example.manageBook.model.Proprietaire;
import com.example.manageBook.repository.ProprietaireRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ProprietaireServiceLog {


    @Resource
    ProprietaireRepository proprietaireRepository;

    private final Map<String, Proprietaire> db;



    public ProprietaireServiceLog(PasswordEncoder passwordEncoder) {
        this.db = new ConcurrentHashMap<>();

        // add demo user
        Proprietaire proprietaire = new Proprietaire();
        proprietaire.setIdentifiant("admin");
        proprietaire.setMdp(passwordEncoder.encode("admin"));
        save(proprietaire);
    }


    public Proprietaire creeUnProprietaire(Proprietaire proprietaire){

       return this.proprietaireRepository.save(proprietaire);

    }

    public Proprietaire lookup(String identifiant) {

        Proprietaire proprietaire = proprietaireRepository.findProprietaireByIdentiant(identifiant);

        return proprietaire;
    }

    public void save(Proprietaire proprietaire) {
        this.db.put(proprietaire.getIdentifiant(), proprietaire);
    }

    public boolean usernameExists(String identifiant) {
        Proprietaire proprietaire = proprietaireRepository.findProprietaireByIdentiant(identifiant);
    if(proprietaire != null){
    return true;
    }else{
        return false;
    }




    }












}

package com.example.manageBook.services.impl;

import com.example.manageBook.model.Emprunte;
import com.example.manageBook.model.Retour;
import com.example.manageBook.repository.EmprunteRepository;
import com.example.manageBook.repository.RetourRepository;
import com.example.manageBook.services.EmprunteService;
import com.example.manageBook.services.RetourService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class RetourServiceImpl implements RetourService {


    @Resource
    RetourRepository retourRepository;


    @Override
    public Retour creeUnRetour(Retour retour){

        Retour retour1 = retourRepository.save(retour);

                return retour1;
    }





}

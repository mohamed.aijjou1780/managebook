package com.example.manageBook.services.impl;

import com.example.manageBook.model.Emprunte;
import com.example.manageBook.model.KeyEmprunte;
import com.example.manageBook.repository.EmprunteRepository;
import com.example.manageBook.services.EmprunteService;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class EmprunteServiceImpl implements EmprunteService {


    @Resource
    EmprunteRepository emprunteRepository;


    @Override
    public Emprunte creeUnEmprunt(Emprunte emprunte){

        Emprunte emprunte1 = emprunteRepository.save(emprunte);

                return emprunte1;
    }


    @Override
    public List<Emprunte> getListEmprunt(){

        Iterable<Emprunte> listemprunte = emprunteRepository.findAll();

        List<Emprunte> emprunteList = new ArrayList<>();

       for ( Emprunte emprunte1 : listemprunte){

           emprunteList.add(emprunte1);

       }

        return emprunteList;
    }

    @Override
    public Emprunte getListEmpruntById(Integer idEmprunte){

        Emprunte emprunte;

        emprunte = emprunteRepository.listemprunteParId(idEmprunte);



        return emprunte;
    }

    @Override
    public Emprunte getEmpruntByIdEmprunteurByIdProduit(Integer idEmprunteur, Integer idProduit){

        Emprunte emprunte;

        emprunte = emprunteRepository.unEmpruntParIdEmprunteurEtIdProduit(idEmprunteur, idProduit);



        return emprunte;
    }

    @Override
    public void supprimeUnEmpruntById(Integer idEmprunte){


    this.emprunteRepository.deleteById(idEmprunte);


    }

    @Override
    public Date getDateEmpruntByIdProduit(Integer idProduit){

        Date dateEmprunte;

        dateEmprunte = emprunteRepository.uneDateEmpruntParIdProduit(idProduit);



        return dateEmprunte;
    }



}

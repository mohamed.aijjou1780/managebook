package com.example.manageBook.services.impl;

import com.example.manageBook.model.Proprietaire;
import com.example.manageBook.repository.ProprietaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;


@Service
public class ProprietaireServiceDetails {


    @Resource
    private ProprietaireRepository proprietaireRepository;



    @Autowired
    private PasswordEncoder bCryptPasswordEncoder;



    public Proprietaire findProprietaireByIdentifiant(String identifiant){

        Proprietaire proprietaire = proprietaireRepository.findProprietaireByIdentiant(identifiant);

        return proprietaire;


    }


    public Integer findIdProprietaireByIdentifiant(String identifiant){

        Integer idProprietaire = proprietaireRepository.findIdProprietaireByIdentiant(identifiant);

        return idProprietaire;


    }

//    public void saveProprietaire (Proprietaire proprietaire){
//
//        proprietaire.setMdp(bCryptPasswordEncoder.encode(proprietaire.getMdp()));
//        Role roleProprietaire = roleRepository.findByRole("ADMIN");
//        proprietaire.setRoles(new HashSet<>(Arrays.asList(roleProprietaire)));
//        this.proprietaireRepository.save(proprietaire);
//
//    }

//    @Override
//    public UserDetails loadUserByUsername(String identifiant) throws UsernameNotFoundException {
//
//        Proprietaire proprietaire = proprietaireRepository.findProprietaireByIdentiant(identifiant);
//        if(proprietaire != null) {
//            List<GrantedAuthority> authorities = getUserAuthority(proprietaire.getRoles());
//            return buildUserForAuthentication(proprietaire, authorities);
//        } else {
//            throw new UsernameNotFoundException("username not found");
//        }
//    }
//    private List<GrantedAuthority> getUserAuthority(Set<Role> userRoles) {
//        Set<GrantedAuthority> roles = new HashSet<>();
//        userRoles.forEach((role) -> {
//            roles.add(new SimpleGrantedAuthority(role.getRole()));
//        });
//
//        List<GrantedAuthority> grantedAuthorities = new ArrayList<>(roles);
//        return grantedAuthorities;
//    }
    private UserDetails buildUserForAuthentication(Proprietaire proprietaire, List<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(proprietaire.getIdentifiant(), proprietaire.getMdp(), authorities);
    }



}

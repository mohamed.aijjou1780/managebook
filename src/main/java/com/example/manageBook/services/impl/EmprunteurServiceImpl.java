package com.example.manageBook.services.impl;


import com.example.manageBook.model.Emprunteur;
import com.example.manageBook.repository.EmprunteurRepository;
import com.example.manageBook.services.EmprunteurService;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class EmprunteurServiceImpl implements EmprunteurService {


    @Resource
    EmprunteurRepository emprunteurRepository;


    @Override
    public List<Emprunteur> listEmprunteur() {
        Iterable<Emprunteur>listemprunteur = emprunteurRepository.findAll();
       List<Emprunteur> listemprunteur1 = new ArrayList<>();

       for (Emprunteur emprunteur : listemprunteur ){

           listemprunteur1.add(emprunteur);

       }

        return listemprunteur1;
    }

    @Override
    public Emprunteur findEmprunteurById(Integer idEmprunteur){

        Emprunteur emprunteur = emprunteurRepository.findEmprunteurParId(idEmprunteur);

        return emprunteur;

}

    @Override
    public Emprunteur findEmprunteurByEmprunteAndProduitById(Integer idProduit){

        Emprunteur emprunteur = emprunteurRepository.findEmprunteurByEmprunteAndProduitById(idProduit);

        return emprunteur;

    }

    @Override
    public Emprunteur findEmprunteurByNomAndByPrenom(String nom, String prenom){

        Emprunteur emprunteur = emprunteurRepository.findEmprunteurByNomAndPrenom(nom,prenom);

        return emprunteur;

    }
    @Override
    public Emprunteur creationEmprunteur(Emprunteur emprunteur){

        Emprunteur emprunteur1 = emprunteurRepository.save(emprunteur);

        return emprunteur1;

    }


}

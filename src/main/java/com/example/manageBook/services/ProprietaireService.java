package com.example.manageBook.services;

import com.example.manageBook.model.Produit;
import com.example.manageBook.model.Proprietaire;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;
import java.util.Optional;

public interface ProprietaireService {

    public Proprietaire creeUnProprietaire(Proprietaire proprietaire);
    public Proprietaire trouveUnProprietaireParId(Integer idProprietaire);


}

package com.example.manageBook.services;

import com.example.manageBook.model.Commente;
import com.example.manageBook.model.Emprunte;

import java.util.List;

public interface CommenteService {



    Commente creeUnCommente(Commente commente);
    List<Commente> getCommenteByIdProduitAndByProprietaire(Integer idProprietaire,Integer idProduit);
    void supprimeUnCommenteById(Integer idCommente);

}

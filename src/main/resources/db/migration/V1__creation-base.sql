
create TABLE proprietaire(
   id  serial PRIMARY KEY,
   nom CHARACTER VARYING(50)  NOT NULL,
   prenom CHARACTER VARYING NOT NULL,
   avatar CHARACTER VARYING NOT NULL,
   description CHARACTER VARYING (2000),
   idenfiant CHARACTER VARYING NOT NULL,
   mdp CHARACTER VARYING (2000)
);

create TABLE produit(
	id  serial PRIMARY KEY,
	titre CHARACTER VARYING (50) NOT NULL,
	auteur CHARACTER VARYING(50) NOT NULL,
	type CHARACTER VARYING(50) NOT NULL,
	image CHARACTER VARYING(1000),
	image2 CHARACTER VARYING(1000),
	ean character varying(50),
	edition CHARACTER VARYING(50),
	description CHARACTER VARYING(2000),
	present CHARACTER VARYING(50) NOT NULL,
	date DATE,
	idproprietaire INTEGER REFERENCES proprietaire
);

create TABLE emprunteur(
	id  serial PRIMARY KEY,
	nom CHARACTER VARYING (50) NOT NULL,
	prenom CHARACTER VARYING (50) NOT NULL,
	lien CHARACTER VARYING (50) NOT NULL
);

create TABLE emprunte(
    id serial  PRIMARY KEY,
    date DATE,
    produit_id INTEGER REFERENCES produit,
    emprunteur_id INTEGER REFERENCES emprunteur
);

create TABLE retour(
	id serial PRIMARY KEY,
	date DATE,
    produit_id INTEGER REFERENCES produit,
    emprunteur_id INTEGER REFERENCES emprunteur
 );

create TABLE commente(
   id serial PRIMARY KEY,
   date DATE,
   note INTEGER,
   commentaire CHARACTER VARYING (2000) NOT NULL,
   proprietaire_id INTEGER REFERENCES proprietaire,
    produit_id INTEGER REFERENCES produit,
   emprunteur_id INTEGER REFERENCES emprunteur

);